#include "simple_log_timer.h"
#include <iostream>

simple_log_timer::simple_log_timer(const char* n)
    : name(n), precision(timer_precision::milliseconds) {
    tp_start = std::chrono::steady_clock::now();
}

simple_log_timer::simple_log_timer(const char* n, timer_precision p)
    : name(n), precision(p) {
    tp_start = std::chrono::steady_clock::now();
}

void simple_log_timer::end() {
    if (name != nullptr) {
        auto tp_end = std::chrono::steady_clock::now();
        if(precision == timer_precision::milliseconds)
            std::cout << name << ": " << std::chrono::duration_cast<std::chrono::milliseconds>(tp_end - tp_start).count() << "ms\n";
        else
            std::cout << name << ": " << std::chrono::duration_cast<std::chrono::nanoseconds>(tp_end - tp_start).count() << "ns\n";
        name = nullptr;
    }
}

simple_log_timer::~simple_log_timer() {
    end();
}
