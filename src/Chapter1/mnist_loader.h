#pragma once

#include <vector>
#include "net_matrix.h"

std::pair<std::vector<net_matrix>, std::vector<net_matrix>> read_mnist(const char* idx3_path, const char* idx1_path, size_t max_items, std::allocator<net_matrix::value_type> allocator);