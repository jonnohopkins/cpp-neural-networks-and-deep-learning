#pragma once

#include <vector>
#include <span>
#include <random>
#include "net_matrix.h"
#include <istream>
#include <ostream>

struct network {
	using allocator_type = std::allocator<net_matrix::value_type>;

	allocator_type allocator;

	std::vector<size_t> layer_sizes;
	// weights.size() == layer_sizes.size() - 1
	// weights[i] has dimensions [ layer_sizes[i], layer_sizes[i+1] ]
	// ie. for i = [1, weights.size()): weights[i - 1].rows == weights[i].columns
	std::vector<net_matrix> weights;
	// biases.size() == layer_sizes.size() - 1
	// biases[i] has dimensions [ 1, layer_sizes[i+1] ] or equivalently [ 1, weights[i].rows ]
	std::vector<net_matrix> biases;

	// initialize every weight and bias cell to default_value
	network(std::span<const size_t> sizes, net_matrix::value_type default_value = net_matrix::value_type{});

	// create weights and biases with specified sizes, cells are uninitialized.
	network(std::span<const size_t> sizes, uninitialized_t);

	// randomize initial weights and biases
	network(std::initializer_list<size_t> sizes, std::mt19937& gen)
		: network(std::span<const size_t>{ sizes }, gen) {
	}

	// randomize initial weights and biases
	network(std::span<const size_t> sizes, std::mt19937& gen);

	size_t num_weights() const {
		size_t count = 0;
		for (const auto& w : weights) {
			count += w.size();
		}

		return count;
	}

	size_t num_biases() const {
		size_t count = 0;
		for (const auto& b : biases) {
			count += b.size();
		}

		return count;
	}

	// including the input layer
	size_t max_layer_size() const {
		size_t max_size = 0;
		for (const auto& s : layer_sizes)
			max_size = std::max(max_size, s);

		return max_size;
	}

	void reset(std::span<const size_t> sizes, net_matrix::value_type default_value = net_matrix::value_type{});
	void reset(std::span<const size_t> sizes, uninitialized_t);
	std::vector<net_matrix> create_activations(size_t start_layer_idx, allocator_type alloc) const;

	// activations[0] must be set to the activation to be fed forward
	// the remaining matrices will become the result
	// 
	// assumes
	//   activations.size() + in_layer_idx < layer_sizes.size()
	//   activations[i] has dimensions [ 1, layer_sizes[i + in_layer_idx] ]
	void feedforward(std::span<const net_matrix> weights_transposed, std::span<net_matrix> activations, size_t in_layer_idx = 0) const;

	void stochastic_gradient_descent(std::span<net_matrix> training_data_input, std::span<net_matrix> training_data_expected,
		size_t epochs, size_t mini_batch_size, float eta, size_t sgd_num_threads, size_t sgd_num_per_thread,
		std::span<const net_matrix> test_data_input, std::span<const net_matrix> test_data_expected, size_t evaluate_num_threads, size_t evaluate_num_per_thread, allocator_type alloc);

	// assumes test_data[test_idx].first has dimensions [ 1, layer_sizes.front() ]
	// returns number classified correctly
	size_t evaluate_accuracy(std::span<const net_matrix> test_data_input, std::span<const net_matrix> test_data_expected, size_t num_threads, size_t num_per_thread, allocator_type alloc) const;

	std::pair<std::vector<net_matrix>, std::vector<net_matrix>> update_mini_batch(
		std::span<net_matrix>::iterator training_batch_input_begin, std::span<net_matrix>::iterator training_batch_input_end,
		std::span<net_matrix>::iterator training_batch_expected_begin,
		size_t mini_batch_size, float eta, allocator_type alloc) const;

	// x is the input
	// y is the expected output.
	// result: delta_nabla_weights and delta_nabla_biases
	// 
	// assumes
	//   x has dimensions [ 1, layer_sizes.front() ]
	//   y has dimensions [ 1, layer_sizes.back() ]
	//
	// we could potentially improve this function by modifying it to work on multiple inputs simultaneously.
	void backpropagation(std::span<net_matrix> delta_nabla_weights, std::span<net_matrix> delta_nabla_biases,
		std::span<const net_matrix> weights_transposed,
		const net_matrix& x, const net_matrix& y, allocator_type alloc) const;

	friend std::ostream& operator<<(std::ostream& os, const network& net);
	friend std::istream& operator>>(std::istream& os, const network& net);
};

std::ostream& operator<<(std::ostream& os, const network& net);

std::istream& operator>>(std::istream& is, network& net);
