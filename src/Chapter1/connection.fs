#version 330 core
in vec2 TexCoord;

out vec4 FragColor;

uniform vec4 color; // ie. the weight color
uniform float activation_position;
uniform float activation_strength;

float easeInOutQuad(float x) {
	return x < 0.5 ? 2.0 * x * x : 1.0 - (-2.0 * x + 2.0) * (-2.0 * x + 2.0) / 2.0;
}

void main()
{
	float distance_middle = easeInOutQuad(clamp(0.1 - 2.0 * abs(activation_position - TexCoord.x), 0.0, 1.0) * 10.0);
	vec4 deactivation_color = color;
	vec4 activation_color = mix(deactivation_color, vec4(1.0, 1.0, 1.0, color.a), activation_strength);
	FragColor = mix(color, activation_color, distance_middle);
}
