#pragma once

#ifdef __AVX2__
#define MATRIX_AVX_FLOAT
#endif
#include "matrix.h"

typedef matrix<float> net_matrix;
typedef owning_matrix<float> owning_net_matrix;
