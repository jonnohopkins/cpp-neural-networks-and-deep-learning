#pragma once

#include <cassert>
#include <algorithm>
#include <memory>
#include <type_traits>
#ifdef MATRIX_AVX_FLOAT
#include <immintrin.h>
#endif

struct uninitialized_t {};
inline constexpr static uninitialized_t uninitialized;

// conforms to https://en.cppreference.com/w/cpp/named_req/Container
template<typename T>
struct matrix {
	// typedefs
	typedef T value_type;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T* iterator;
	typedef const T* const_iterator;
	typedef std::ptrdiff_t difference_type;
	typedef size_t size_type;

	// members
	size_t columns;
	size_t rows;
	value_type* cells; // stored in column-major order. non-owning pointer.

	// constructors / destructors
	explicit matrix() noexcept
		: columns(0),
		rows(0),
		cells(nullptr) {
	}

	matrix(size_t col, size_t row, value_type* c)
		: columns(col),
		rows(row),
		cells(c) {
	}

	matrix(const matrix<value_type>&) = delete;
	matrix(matrix<value_type>&&) = default;

	// assumes
	//   this has same dimensions as other
	matrix& operator=(const matrix<value_type>& other) {
		assert(columns == other.columns);
		assert(rows == other.rows);
		assert(static_cast<bool>(cells) == static_cast<bool>(other.cells));

		std::copy(other.begin(), other.end(), begin());

		return *this;
	}

	matrix& operator=(matrix<value_type>&&) = default;

	// container functions
	inline iterator begin() { return cells; }
	inline iterator end() { return cells + size(); }
	inline const_iterator begin() const { return cells; }
	inline const_iterator end() const { return cells + size(); }
	inline const_iterator cbegin() const { return cells; }
	inline const_iterator cend() const { return cells + size(); }

	// aka. the number of cells
	inline size_type size() const { return columns * rows; }
	inline bool empty() const { return size() == 0; }

	// functions
	inline value_type& at(size_t x, size_t y) {
		assert(x < columns);
		assert(y < rows);
		return *(cells + x * rows + y);
	}

	inline const value_type& at(size_t x, size_t y) const {
		assert(x < columns);
		assert(y < rows);
		return *(cells + x * rows + y);
	}

	template<typename F>
	matrix<value_type>& element_transform(F func) {
		for (auto i = 0; i < size(); ++i) {
			auto& cell = *(cells + i);
			cell = func(cell);
		}

		return *this;
	}

#ifdef MATRIX_AVX_FLOAT
	template<typename F, typename G>
	matrix<value_type>& element_transform_avx(F avx_func, G func) {
		auto i = 0;
		for (; i + 7 < size(); i += 8) {
			__m256* cell = reinterpret_cast<__m256*>(cells + i);
			_mm256_store_ps(cells + i, avx_func(*cell));
		}

		for (; i < size(); ++i) {
			auto& cell = *(cells + i);
			cell = func(cell);
		}

		return *this;
	}
#endif

	// assumes
	//   this has same dimensions as mat
	template<typename U, typename F>
	matrix<value_type>& set_element_transform(const matrix<U>& mat, F func) {
		assert(columns == mat.columns);
		assert(rows == mat.rows);
		for (auto i = 0; i < size(); ++i) {
			*(cells + i) = func(*(mat.cells + i));
		}

		return *this;
	}

#ifdef MATRIX_AVX_FLOAT
	// assumes
	//   this has same dimensions as mat
	template<typename U, typename F, typename G>
	matrix<value_type>& set_element_transform_avx(const matrix<U>& mat, F avx_func, G func) {
		assert(columns == mat.columns);
		assert(rows == mat.rows);
		auto i = 0;
		for (; i + 7 < size(); i += 8) {
			__m256* cell = reinterpret_cast<__m256*>(mat.cells + i);
			_mm256_store_ps(cells + i, avx_func(*cell));
		}
		for (; i < size(); ++i) {
			*(cells + i) = func(*(mat.cells + i));
		}

		return *this;
	}
#endif

	// assumes
	//   this has same dimensions as rhs
	matrix<value_type>& element_multiply_assign(const matrix<value_type>& rhs) {
		assert(columns == rhs.columns);
		assert(rows == rhs.rows);

		auto i = 0;
#ifdef MATRIX_AVX_FLOAT
		for (; i + 7 < size(); i += 8) {
			__m256* this_cell = reinterpret_cast<__m256*>(cells + i);
			const __m256* rhs_cell = reinterpret_cast<const __m256*>(rhs.cells + i);
			_mm256_store_ps(cells + i, _mm256_mul_ps(*this_cell, *rhs_cell));
		}
#endif
		for (; i < size(); ++i) {
			*(cells + i) *= *(rhs.cells + i);
		}

		return *this;
	}

	// todo: use avx if available
	// 
	// assumes
	//   this has same dimensions as rhs
	template<typename U, typename F>
	matrix<value_type>& element_combine_assign(const matrix<U>& rhs, F func) {
		assert(columns == rhs.columns);
		assert(rows == rhs.rows);
		for (auto i = 0; i < size(); ++i) {
			auto& cell = *(cells + i);
			cell = func(cell, *(rhs.cells + i));
		}

		return *this;
	}

	matrix<value_type>& transpose() {
		if (rows == columns) {
			const size_t length = columns;

			for (size_t i = 0; i < length; ++i) {
				for (size_t j = i + 1; j < length; ++j) {
					std::swap(at(i, j), at(j, i));
				}
			}
		}
		else if (size() > 0) {
			auto new_idx_from_old = [this](size_t old_idx) {
				size_t old_x = old_idx / rows, old_y = old_idx % rows;
				return old_y * columns + old_x;
				};

			size_t idx = 1;
			value_type value = *(cells + idx);

			// cells along the diagonal (ie, [i, i]) don't need to be moved.
			// this loop uses a minimal number of temporary values to shuffle all other elements to their new location.
			// we are guaranteed to return to our first index when we loop in the following way.
			do {
				idx = new_idx_from_old(idx);
				std::swap(*(cells + idx), value);
			} while (idx != 1);

			std::swap(columns, rows);
		}

		return *this;
	}

	// assumes
	//   this != &mat
	//   this has dimensions [ mat.rows, mat.columns ]
	template<typename U>
	matrix<value_type>& set_transpose(const matrix<U>& mat) {
		assert(this != &mat);

		for (auto i = 0; i < columns; ++i) {
			for (auto j = 0; j < rows; ++j) {
				at(i, j) = mat.at(j, i);
			}
		}

		return *this;
	}

	// use set_transpose_multiply() for faster SIMD multiplication.
	// 
	// assumes
	//   this != &lhs
	//   this != &rhs
	//   lhs.columns == rhs.rows
	//   this has dimensions [ rhs.columns, lhs.rows ]
	matrix<value_type>& set_multiply(const matrix<value_type>& lhs, const matrix<value_type>& rhs) {
		assert(this != &lhs);
		assert(this != &rhs);
		assert(columns == rhs.columns);
		assert(rows == lhs.rows);
		assert(lhs.columns == rhs.rows);

		auto matching_size = lhs.columns;

		for (auto x = 0; x < columns; ++x) {
			for (auto y = 0; y < rows; ++y) {
				auto& cell = at(x, y);
				cell = (value_type)0;
				for (auto k = 0; k < matching_size; ++k) {
					cell += lhs.at(k, y) * rhs.at(x, k);
				}
			}
		}

		return *this;
	}

	// sets *this = transpose(lhs) * rhs
	// 
	// assumes
	//   this != &lhs
	//   this != &rhs
	//   lhs.rows == rhs.rows
	//   this has dimensions [ rhs.columns, lhs.columns ]
	matrix<value_type>& set_transpose_multiply(const matrix<value_type>& lhs, const matrix<value_type>& rhs) {
		assert(this != &lhs);
		assert(this != &rhs);
		assert(columns == rhs.columns);
		assert(rows == lhs.columns);
		assert(lhs.rows == rhs.rows);

		auto matching_size = lhs.rows;

		for (auto x = 0; x < columns; ++x) {
			for (auto y = 0; y < rows; ++y) {
				auto& cell = at(x, y);
				cell = (value_type)0;

				auto k = 0;
#ifdef MATRIX_AVX_FLOAT
				__m256 intermediate = _mm256_setzero_ps();
				for (; k + 7 < matching_size; k += 8) {
					const __m256* lhs_cell = reinterpret_cast<const __m256*>(&lhs.at(y, k));
					const __m256* rhs_cell = reinterpret_cast<const __m256*>(&rhs.at(x, k));
					intermediate = _mm256_fmadd_ps(*lhs_cell, *rhs_cell, intermediate);
				}

				intermediate = _mm256_hadd_ps(_mm256_permute2f128_ps(intermediate, intermediate, 0x20), _mm256_permute2f128_ps(intermediate, intermediate, 0x31));
				intermediate = _mm256_hadd_ps(_mm256_permute2f128_ps(intermediate, intermediate, 0x20), _mm256_permute2f128_ps(intermediate, intermediate, 0x31));
				intermediate = _mm256_hadd_ps(_mm256_permute2f128_ps(intermediate, intermediate, 0x20), _mm256_permute2f128_ps(intermediate, intermediate, 0x31));
				cell += *reinterpret_cast<float*>(&intermediate);
#endif

				for (; k < matching_size; ++k) {
					cell += lhs.at(y, k) * rhs.at(x, k);
				}
			}
		}

		return *this;
	}
};

// todo: use avx if available
template<typename T>
bool operator ==(const matrix<T>& lhs, const matrix<T>& rhs) {
	return lhs.columns == rhs.columns && lhs.rows == rhs.rows &&
		std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template<typename T>
bool operator !=(const matrix<T>& lhs, const matrix<T>& rhs) {
	return !(lhs == rhs);
}

// assumes
//   lhs and rhs have equal dimensions
template<typename T>
matrix<T>& operator +=(matrix<T>& lhs, const matrix<T>& rhs) {
	assert(lhs.columns == rhs.columns);
	assert(lhs.rows == rhs.rows);

	auto i = 0;
#ifdef MATRIX_AVX_FLOAT
	for (; i + 7 < lhs.size(); i += 8) {
		__m256* lhs_cell = reinterpret_cast<__m256*>(lhs.cells + i);
		const __m256* rhs_cell = reinterpret_cast<const __m256*>(rhs.cells + i);
		_mm256_store_ps(lhs.cells + i, _mm256_add_ps(*lhs_cell, *rhs_cell));
	}
#endif
	for (; i < lhs.size(); ++i) {
		*(lhs.cells + i) += *(rhs.cells + i);
	}

	return lhs;
}

// assumes
//   lhs and rhs have equal dimensions
template<typename T>
matrix<T>& operator -=(matrix<T>& lhs, const matrix<T>& rhs) {
	assert(lhs.columns == rhs.columns);
	assert(lhs.rows == rhs.rows);

	auto i = 0;
#ifdef MATRIX_AVX_FLOAT
	for (; i + 7 < lhs.size(); i += 8) {
		__m256* lhs_cell = reinterpret_cast<__m256*>(lhs.cells + i);
		const __m256* rhs_cell = reinterpret_cast<const __m256*>(rhs.cells + i);
		_mm256_store_ps(lhs.cells + i, _mm256_sub_ps(*lhs_cell, *rhs_cell));
	}
#endif
	for (; i < lhs.size(); ++i) {
		*(lhs.cells + i) -= *(rhs.cells + i);
	}

	return lhs;
}

// assumes
//   lhs.columns == 1
//   rhs.columns == 1
//   lhs.rows == rhs.rows
template<typename T, typename A>
T dot(const matrix<T>& lhs, const matrix<T>& rhs) {
	assert(lhs.columns == 1);
	assert(rhs.columns == 1);
	assert(lhs.rows == rhs.rows);

	auto result = (T)0;
	size_t y = 0;

#ifdef MATRIX_AVX_FLOAT
	__m256 intermediate = _mm256_setzero_ps();
	for (; y + 7 < lhs.rows; ++y) {
		const __m256* lhs_cell = reinterpret_cast<const __m256*>(&lhs.at(0, y));
		const __m256* rhs_cell = reinterpret_cast<const __m256*>(&rhs.at(0, y));
		intermediate = _mm256_fmadd_ps(*lhs_cell, *rhs_cell, intermediate);
	}

	intermediate = _mm256_hadd_ps(_mm256_permute2f128_ps(intermediate, intermediate, 0x20), _mm256_permute2f128_ps(intermediate, intermediate, 0x31));
	intermediate = _mm256_hadd_ps(_mm256_permute2f128_ps(intermediate, intermediate, 0x20), _mm256_permute2f128_ps(intermediate, intermediate, 0x31));
	intermediate = _mm256_hadd_ps(_mm256_permute2f128_ps(intermediate, intermediate, 0x20), _mm256_permute2f128_ps(intermediate, intermediate, 0x31));
	result = *reinterpret_cast<float*>(&intermediate);
#endif

	for (; y < lhs.rows; ++y) {
		result += lhs.at(0, y) * rhs.at(0, y);
	}

	return result;
}

template<typename T, typename A = std::allocator<T>>
struct owning_matrix {
	// typedefs
	typedef T value_type;
	typedef A allocator_type;

	// members
	allocator_type allocator;
	matrix<T> mat;

	// constructors / destructors
	explicit owning_matrix(allocator_type a = allocator_type{}) noexcept
		: allocator(a),
		mat() {
	}

	owning_matrix(size_t col, size_t row, uninitialized_t, allocator_type a = allocator_type{})
		: allocator(a),
		mat(col, row, allocator.allocate(col * row)) {
	}

	owning_matrix(size_t col, size_t row, value_type default_value = 0, allocator_type a = allocator_type{})
		: allocator(a),
		mat(col, row, allocator.allocate(col * row)) {
		for (auto& cell : mat)
			cell = default_value;
	}

	owning_matrix(const owning_matrix<value_type, allocator_type>& other)
		: allocator(other.allocator),
		mat(other.columns, other.rows, allocator.allocate(other.size())) {
		std::copy(mat.begin(), mat.end(), mat.begin());
	}

	owning_matrix(owning_matrix<value_type, allocator_type>&& other) noexcept
		: allocator(std::move(other.allocator)),
		mat(std::move(other.mat)) {
		other.mat.cells = nullptr;
	}

	owning_matrix<value_type, allocator_type>& operator =(const owning_matrix<value_type, allocator_type>& rhs) {
		if (this != &rhs) {
			if constexpr (std::allocator_traits<allocator_type>::propagate_on_container_copy_assignment::value) {
				if (allocator != rhs.allocator || mat.size() != rhs.mat.size()) {
					allocator.deallocate(mat.cells, mat.size());
					allocator = rhs.allocator;
					mat.cells = allocator.allocate(rhs.mat.size());
				}
			}
			else if (mat.size() != rhs.mat.size()) {
				allocator.deallocate(mat.cells, mat.size());
				mat.cells = allocator.allocate(rhs.mat.size());
			}

			std::copy(rhs.mat.begin(), rhs.mat.end(), mat.begin());
			mat.columns = rhs.mat.columns;
			mat.rows = rhs.mat.rows;
		}

		return *this;
	}

	owning_matrix<value_type, allocator_type>& operator =(owning_matrix<value_type, allocator_type>&& rhs) noexcept {
		if (this != &rhs) {
			if constexpr (std::allocator_traits<allocator_type>::propagate_on_container_move_assignment::value) {
				allocator.deallocate(mat.cells, mat.size());
				allocator = std::move(rhs.allocator);
				mat.cells = rhs.mat.cells;
				mat.columns = rhs.mat.columns;
				mat.rows = rhs.mat.rows;

				rhs.mat.cells = nullptr;
			}
			else {
				if (mat.size() != rhs.mat.size()) {
					allocator.deallocate(mat.cells, mat.size());
					mat.cells = allocator.allocate(rhs.mat.size());
				}

				std::copy(rhs.mat.begin(), rhs.mat.end(), mat.begin());
				mat.columns = rhs.mat.columns;
				mat.rows = rhs.mat.rows;
			}
		}

		return *this;
	}

	~owning_matrix() {
		if (mat.cells)
			allocator.deallocate(mat.cells, mat.size());
	}

	// only reallocates if the sizes are different
	void reset(size_t new_columns, size_t new_rows, uninitialized_t) {
		auto new_size = new_columns * new_rows;
		if (mat.size() != new_size) {
			allocator.deallocate(mat.cells, mat.size());
			mat.cells = allocator.allocate(new_size);
		}
		mat.columns = new_columns;
		mat.rows = new_rows;
	}

	void swap(owning_matrix<value_type, allocator_type>& rhs) {
		if (this != &rhs) {
			using std::swap;
			if constexpr (std::allocator_traits<allocator_type>::propagate_on_container_swap::value) {
				swap(allocator, rhs.allocator);
				swap(mat.columns, rhs.mat.columns);
				swap(mat.rows, rhs.mat.rows);
				swap(mat.cells, rhs.mat.cells);
			}
			else {
				if (allocator == rhs.allocator) {
					swap(mat.cells, rhs.mat.cells);
					swap(mat.columns, rhs.mat.columns);
					swap(mat.rows, rhs.mat.rows);
				}
				else {
					if (mat.size() == rhs.mat.size()) {
						swap(mat.cells, rhs.mat.cells);
					}
					else {
						value_type* temp_cells = allocator.allocate(rhs.mat.size());
						std::copy(rhs.mat.begin(), rhs.mat.end(), temp_cells);

						rhs.allocator.deallocate(rhs.mat.cells, rhs.mat.size());
						rhs.mat.cells = allocator.allocate(mat.size());
						std::copy(mat.begin(), mat.end(), rhs.mat.cells);

						allocator.deallocate(mat.cells, mat.size());
						mat.cells = temp_cells;
					}

					swap(mat.columns, rhs.mat.columns);
					swap(mat.rows, rhs.mat.rows);
				}
			}
		}
	}
};

template<typename T, typename A>
void swap(owning_matrix<T, A>& lhs, owning_matrix<T, A>& rhs) {
	lhs.swap(rhs);
}
