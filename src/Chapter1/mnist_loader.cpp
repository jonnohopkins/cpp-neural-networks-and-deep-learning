#include "mnist_loader.h"
#include <fstream>
#include <iostream>
#include <cassert>
#include <algorithm>

#if _WIN32
#define NOMINMAX
#include <winsock.h>
#endif

template<typename T>
T read(std::istream& is);

template<>
uint32_t read<uint32_t>(std::istream& is) {
	uint32_t value;
	is.read(reinterpret_cast<char*>(&value), sizeof(value));
	value = ntohl(value);
	return value;
}

template<>
uint8_t read<uint8_t>(std::istream& is) {
	uint8_t value;
	is.read(reinterpret_cast<char*>(&value), sizeof(value));
	return value;
}

std::pair<std::vector<net_matrix>, std::vector<net_matrix>> read_mnist(const char* idx3_path, const char* idx1_path, size_t max_items, std::allocator<net_matrix::value_type> allocator) {
	std::pair<std::vector<net_matrix>, std::vector<net_matrix>> result;

	std::ifstream idx3;
	idx3.open(idx3_path, std::ios::in | std::ios::binary);

	std::ifstream idx1;
	idx1.open(idx1_path, std::ios::in | std::ios::binary);

	if (!idx3.is_open()) {
		std::cerr << "idx3 file could not be opened." << std::endl;
		return {};
	}
	else if (!idx1.is_open()) {
		std::cerr << "idx1 file could not be opened." << std::endl;
		return {};
	}

	auto idx3_header = read<uint32_t>(idx3);
	(void)idx3_header;
	assert(idx3_header == 0x00000803);
	auto idx3_num_items = read<uint32_t>(idx3);
	(void)idx3_num_items;
	size_t num_rows = read<uint32_t>(idx3);
	size_t num_columns = read<uint32_t>(idx3);

	auto idx1_header = read<uint32_t>(idx1);
	(void)idx1_header;
	assert(idx1_header == 0x00000801);
	auto idx1_num_items = read<uint32_t>(idx1);

	assert(idx1_num_items == idx3_num_items);

	auto num_items = std::min((size_t) idx1_num_items, max_items);

	result.first.reserve(num_items);
	for (auto i = 0; i < num_items; ++i)
		result.first.push_back(net_matrix{ 1, num_columns * num_rows, allocator.allocate(num_columns * num_rows) });
	result.second.reserve(num_items);
	for (auto i = 0; i < num_items; ++i) {
		net_matrix mat{ 1, 10, allocator.allocate(10) };
		for (auto& cell : mat)
			new (&cell) net_matrix::value_type{ 0 };
		result.second.push_back(std::move(mat));
	}

	for (auto i = 0; i < num_items; ++i) {
		auto& image = result.first[i];
		auto& expected_output = result.second[i];

		for (auto idx_column = 0; idx_column < num_columns; ++idx_column) {
			for (auto idx_row = 0; idx_row < num_rows; ++idx_row) {
				image.at(0, idx_column * num_rows + idx_row) = read<uint8_t>(idx3) / 255.0f;
			}
		}

		expected_output.at(0, read<uint8_t>(idx1)) = 1.0f;
	}

	return result;
}