#pragma once

#include <chrono>

enum class timer_precision {
    milliseconds,
    nanoseconds,
};

struct simple_log_timer {
    timer_precision precision;
    std::chrono::steady_clock::time_point tp_start;
    const char* name;

    explicit simple_log_timer(const char* n);
    simple_log_timer(const char* n, timer_precision p);

    void end();

    ~simple_log_timer();
};
