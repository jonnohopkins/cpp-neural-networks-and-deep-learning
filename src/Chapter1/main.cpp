#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "misc/cpp/imgui_stdlib.h"

#pragma warning(push)
#pragma warning(disable : 4201) // nonstandard extension used: nameless struct/union
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>
#pragma warning(pop)

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "net.h"
#include "mnist_loader.h"
#include "simple_log_timer.h"

#include "learnopengl/shader_s.h"

#include <iostream>
#include <filesystem>
#include <chrono>
#include <fstream>
#include <random>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_scroll_callback(GLFWwindow* window, double x, double y);
void mouse_cursor_pos_callback(GLFWwindow* window, double xpos, double ypos);
void processInput(GLFWwindow* window, bool* show_demo_window);

std::allocator<net_matrix::value_type> allocator;

const unsigned int start_window_width = 1080;
const unsigned int start_window_height = 1080;
glm::vec2 window_dimensions;

int percent_weights_shown = 5; // ie. 5%

const size_t image_length = 28;

const float reference_pixels_per_neuron = 20.0f;

const float scroll_sensitivity = 0.1f;
float zoom_factor = 1.0f;

bool first_mouse = true;
glm::vec2 mouse_last_pos;
glm::vec2 mouse_pan;

void glfw_error_callback(int error, const char* description);

typedef unsigned int uint;

const float connection_width = 0.1f;
const float connection_alpha = 0.15f;
const float x_spacing = 12.0f, y_spacing = 1.0f;

typedef std::tuple<glm::mat4, size_t, size_t, size_t, float> neuron_connection_matrices_tuple_t;

glm::vec3 position_for_neuron(float max_layer_size, float x, float y, float layer_size) {
    return glm::vec3(
        0.5f * x_spacing + 0.5f + x * (1.0f + x_spacing),
        0.5f * y_spacing + 0.5f + (y + (max_layer_size - layer_size) / 2.0f) * (1.0f + y_spacing),
        0.0f);
};

void create_connection_matrices(std::vector<neuron_connection_matrices_tuple_t>& neuron_connection_matrices, const network& nn, float max_layer_size) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<size_t> distr;
    typedef std::uniform_int_distribution<size_t>::param_type param_t;

    neuron_connection_matrices.clear();
    neuron_connection_matrices.reserve(nn.num_weights() * percent_weights_shown / 100);

    // because some weight layers may have drastically larger numbers of weights compared to others we prefer to show as many weights as we can evenly across all layers
    // and then distribute the rest among layers with larger numbers. this ends up with a much better display, without much visible difference
    // other than reducing the cluttered look of big layers.

    std::vector<std::pair<size_t, size_t>> indices_to_num_weights;
    indices_to_num_weights.reserve(nn.weights.size());
    for (size_t from_x = 0; from_x < nn.weights.size(); ++from_x) {
        indices_to_num_weights.emplace_back(from_x, nn.weights[from_x].size());
    }

    std::sort(indices_to_num_weights.begin(), indices_to_num_weights.end(), [](const std::pair<size_t, size_t>& lhs, const std::pair<size_t, size_t>& rhs) { return lhs.second < rhs.second; });

    // update indices_to_num_weights to contain the number of weights to display rather than the total number.
    // iterate throught the layers from smallest number of weights to largest, reserving weights across all remaining layers
    size_t unallocated = nn.num_weights() * percent_weights_shown / 100;
    size_t prev_alloc_per_layer = 0;
    size_t i = 0;
    for (; unallocated > 0 && i < indices_to_num_weights.size(); ++i) {
        auto requested_allocation = (indices_to_num_weights[i].second - prev_alloc_per_layer) * (indices_to_num_weights.size() - i);
        if (requested_allocation <= unallocated) {
            unallocated -= requested_allocation;
            prev_alloc_per_layer = indices_to_num_weights[i].second;
        }
        else {
            prev_alloc_per_layer += unallocated / (indices_to_num_weights.size() - i);
            unallocated = 0;
            indices_to_num_weights[i].second = prev_alloc_per_layer;
        }
    }

    // if (unallocated == 0)
    for (; i < indices_to_num_weights.size(); ++i) {
        indices_to_num_weights[i].second = prev_alloc_per_layer;
    }

    // now that indices_to_num_weights contains the number of weights to display, sort it again to the order we are going to iterate over the layers
    std::sort(indices_to_num_weights.begin(), indices_to_num_weights.end(), [](const std::pair<size_t, size_t>& lhs, const std::pair<size_t, size_t>& rhs) { return lhs.first < rhs.first; });

    for (size_t from_x = 0; from_x < nn.weights.size(); ++from_x) {
        const auto to_x = from_x + 1;
        const auto from_layer_size = nn.weights[from_x].columns;
        const auto to_layer_size = nn.weights[from_x].rows;
        for (size_t from_y = 0; from_y < from_layer_size; ++from_y) {
            for (size_t to_y = 0; to_y < to_layer_size; ++to_y) {
                auto chance = indices_to_num_weights[from_x].second;
                if (from_layer_size * to_layer_size == chance || distr(gen, param_t(1, from_layer_size * to_layer_size)) <= chance) {
                    auto from_pos = position_for_neuron(static_cast<float>(max_layer_size ), static_cast<float>( from_x ), static_cast<float>( from_y ), static_cast<float>( from_layer_size ));
                    auto to_pos = position_for_neuron(static_cast<float>( max_layer_size ), static_cast<float>( to_x ), static_cast<float>( to_y ), static_cast<float>( to_layer_size ));

                    auto connection_pos = (from_pos + to_pos) / 2.0f;
                    auto connection_scale = glm::vec3(glm::length(to_pos - from_pos), connection_width, 1.0f);
                    auto connection_rotate = glm::atan((to_pos - from_pos).y, (to_pos - from_pos).x);

                    glm::mat4 mat_model = glm::mat4(1.0f);
                    mat_model = glm::translate(mat_model, connection_pos);
                    mat_model = glm::rotate(mat_model, connection_rotate, glm::vec3(0.0, 0.0, 1.0));
                    mat_model = glm::scale(mat_model, connection_scale);

                    auto weight = nn.weights[from_x].at(from_y, to_y);
                    neuron_connection_matrices.emplace_back(std::move(mat_model), from_x, from_y, to_y, weight);
                }
            }
        }
    }
}

void attempt_open_network(const char* cstr, network& nn, std::vector<net_matrix>& nn_activations, std::vector<neuron_connection_matrices_tuple_t>& neuron_connection_matrices,
    size_t num_layers, size_t max_layer_size) {
    std::ifstream nn_istream{ cstr, std::ios::binary };
    if (!nn_istream.is_open()) {
        std::cerr << "file could not be opened." << std::endl;
    }
    else {
        nn_istream >> nn;
        nn_activations = nn.create_activations(0, allocator);
        num_layers = nn.layer_sizes.size();
        max_layer_size = nn.max_layer_size();
        create_connection_matrices(neuron_connection_matrices, nn, static_cast<float>(max_layer_size));
    }
}

void attempt_save_network(const char* cstr, const network& nn) {
    std::ofstream nn_ostream{ cstr, std::ios::binary };
    if (!nn_ostream.is_open()) {
        std::cerr << "file could not be saved." << std::endl;
    }
    else {
        nn_ostream << nn;
    }
}

void create_window(bool* p_open, std::vector<size_t>& create_sizes, network& nn, std::vector<net_matrix>& nn_activations, std::vector<neuron_connection_matrices_tuple_t>& neuron_connection_matrices,
    size_t num_layers, size_t max_layer_size) {
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize;
    if (ImGui::Begin("New", p_open, window_flags)) {
        ImGui::Text("Dimensions");
        ImGui::Separator();

        auto it_sizes = create_sizes.begin();
        ImGui::Text("%d", *it_sizes);
        ++it_sizes;

        for (int i = 1; i < create_sizes.size() - 1; ++i) {
            ImGui::PushID(i);
            const float button_size = ImGui::GetFrameHeight();
            int value = static_cast<int>(*it_sizes);
            ImGui::DragInt("##value", &value, 1.0f, 0, INT_MAX);
            *it_sizes = value;
            ImGui::SameLine();
            bool add_or_delete = false;
            if (i != 1) {
                if (ImGui::Button("-", ImVec2(button_size, button_size))) {
                    it_sizes = create_sizes.erase(it_sizes);
                    add_or_delete = true;
                }
            }
            ImGui::SameLine();
            if (ImGui::Button("+", ImVec2(button_size, button_size))) {
                it_sizes = create_sizes.insert(it_sizes + 1, *it_sizes);
                add_or_delete = true;
            }

            if (!add_or_delete)
                ++it_sizes;
            ImGui::PopID();
        }

        ImGui::Text("%d", *it_sizes);

        if (ImGui::Button("Create")) {
            nn = network(create_sizes);
            nn_activations = nn.create_activations(0, allocator);
            num_layers = nn.layer_sizes.size();
            max_layer_size = nn.max_layer_size();
            create_connection_matrices(neuron_connection_matrices, nn, static_cast<float>(max_layer_size));
        }

        ImGui::End();
    }
}

void open_window(bool* p_open, std::string* open_path, network& nn, std::vector<net_matrix>& nn_activations, std::vector<neuron_connection_matrices_tuple_t>& neuron_connection_matrices,
    size_t num_layers, size_t max_layer_size) {
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize;
    if (ImGui::Begin("Open", p_open, window_flags)) {
        ImGui::InputText("", open_path);
        ImGui::SameLine();
        if (ImGui::Button("Open")) {
            attempt_open_network(open_path->c_str(), nn, nn_activations, neuron_connection_matrices, num_layers, max_layer_size);
            nn_activations = nn.create_activations(0, allocator);
            num_layers = nn.layer_sizes.size();
            max_layer_size = nn.max_layer_size();
            create_connection_matrices(neuron_connection_matrices, nn, static_cast<float>(max_layer_size));
        }
        ImGui::End();
    }
}

void save_window(bool* p_open, std::string* save_path, const network& nn) {
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize;
    if (ImGui::Begin("Save", p_open, window_flags)) {
        ImGui::InputText("", save_path);
        ImGui::SameLine();
        if(ImGui::Button("Save"))
            attempt_save_network(save_path->c_str(), nn);
        ImGui::End();
    }
}

void training_window(bool* p_open, network& nn,
    std::vector<net_matrix>& training_data_input, std::vector<net_matrix>& training_data_expected,
    std::vector<net_matrix>& test_data_input, std::vector<net_matrix>& test_data_expected,
    std::vector<net_matrix>& validation_data_input, std::vector<net_matrix>& validation_data_expected,
    int* epochs, int* mini_batch_size, float* eta, int* num_backprop_threads, int* num_backprop_per_thread, int* num_evaluate_threads, int* num_feedforward_per_thread) {

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize;
    if (ImGui::Begin("Training", p_open, window_flags)) {
        ImGui::InputInt("#Epochs", epochs);
        ImGui::InputInt("Mini Batch Size", mini_batch_size);
        ImGui::InputFloat("Learning Rate (eta)", eta);
        ImGui::InputInt("#Backprop Threads", num_backprop_threads);
        ImGui::InputInt("#Backprop Per Thread", num_backprop_per_thread);
        ImGui::InputInt("#Evaluate Threads", num_evaluate_threads);
        ImGui::InputInt("#Feedforward Per Thread", num_feedforward_per_thread);

        ImGui::Separator();

        if (ImGui::Button("Train")) {
            simple_log_timer nn_timer("train neural network");
            nn.stochastic_gradient_descent(training_data_input, training_data_expected, *epochs, *mini_batch_size, *eta, *num_backprop_threads, *num_backprop_per_thread,
                test_data_input, test_data_expected, *num_evaluate_threads, *num_feedforward_per_thread, nn.allocator);
            nn_timer.end();
        }

        if (ImGui::Button("Evaluate")) {
            simple_log_timer nn_timer("evaluate neural network");
            auto num_correct = nn.evaluate_accuracy(validation_data_input, validation_data_expected, *num_evaluate_threads, *num_feedforward_per_thread, allocator);
            nn_timer.end();
            std::cout << "Classified " << num_correct << " out of " << validation_data_input.size() << " correctly in validation set.\n";
        }

        ImGui::End();
    }
}

void data_window(bool* p_open,
    std::vector<net_matrix>& training_data_input, std::vector<net_matrix>& training_data_expected,
    std::vector<net_matrix>& test_data_input, std::vector<net_matrix>& test_data_expected,
    std::vector<net_matrix>& validation_data_input, std::vector<net_matrix>& validation_data_expected,
    int* max_mnist_items, int* training_size, bool& has_data_loaded) {

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize;
    if (ImGui::Begin("Data", p_open, window_flags)) {
        ImGui::InputInt("#Items", max_mnist_items);
        ImGui::InputInt("Training Size", training_size);

        if (ImGui::Button("Load")) {
            simple_log_timer mnist_timer("mnist data");

            test_data_input.clear();
            test_data_expected.clear();
            training_data_input.clear();
            training_data_expected.clear();
            validation_data_input.clear();
            validation_data_expected.clear();

            auto [ mnist_input, mnist_expected ] = read_mnist("./resources/mnist/train-images.idx3-ubyte", "./resources/mnist/train-labels.idx1-ubyte", *max_mnist_items, allocator);

            if (!mnist_input.empty() && !mnist_expected.empty()) {
                test_data_input.insert(test_data_input.end(), std::make_move_iterator(mnist_input.begin() + *training_size), std::make_move_iterator(mnist_input.end()));
                test_data_expected.insert(test_data_expected.end(), std::make_move_iterator(mnist_expected.begin() + *training_size), std::make_move_iterator(mnist_expected.end()));
                training_data_input.insert(training_data_input.end(), std::make_move_iterator(mnist_input.begin()), std::make_move_iterator(mnist_input.begin() + *training_size));
                training_data_expected.insert(training_data_expected.end(), std::make_move_iterator(mnist_expected.begin()), std::make_move_iterator(mnist_expected.begin() + *training_size));

                std::tie(validation_data_input, validation_data_expected) = read_mnist("./resources/mnist/t10k-images.idx3-ubyte", "./resources/mnist/t10k-labels.idx1-ubyte", std::numeric_limits<size_t>::max(), allocator);

                has_data_loaded = true;
            }
            else {
                std::cout << "Failed to read MNIST data." << std::endl;
            }
        }

        ImGui::End();
    }
}

ImGuiWindowFlags imgui_window_overlay() {
    const float PAD = 10.0f;
    static int corner = 0;
    //ImGuiIO& io = ImGui::GetIO();
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav;
    if (corner != -1) {
        const ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImVec2 work_pos = viewport->WorkPos; // Use work area to avoid menu-bar/task-bar, if any!
        ImVec2 work_size = viewport->WorkSize;
        ImVec2 window_pos, window_pos_pivot;
        window_pos.x = (corner & 1) ? (work_pos.x + work_size.x - PAD) : (work_pos.x + PAD);
        window_pos.y = (corner & 2) ? (work_pos.y + work_size.y - PAD) : (work_pos.y + PAD);
        window_pos_pivot.x = (corner & 1) ? 1.0f : 0.0f;
        window_pos_pivot.y = (corner & 2) ? 1.0f : 0.0f;
        ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
        window_flags |= ImGuiWindowFlags_NoMove;
    }
    ImGui::SetNextWindowBgAlpha(0.35f); // Transparent background

    return window_flags;
}

void info_overlay(bool* p_open, const network& nn, std::vector<neuron_connection_matrices_tuple_t>& neuron_connection_matrices, float max_layer_size) {
    auto window_flags = imgui_window_overlay();
    if (ImGui::Begin("Neural Net Info", p_open, window_flags)) {
        static ImGuiTextBuffer dimensions_text_builder;
        dimensions_text_builder.clear();
        dimensions_text_builder.append("Dimensions: ");
        if (!nn.weights.empty()) {
            dimensions_text_builder.appendf("%d", nn.weights.front().columns);
            for (const auto& w : nn.weights)
                dimensions_text_builder.appendf(", %d", w.rows);
        }
        else {
            dimensions_text_builder.append("none");
        }

        ImGui::TextUnformatted(dimensions_text_builder.begin(), dimensions_text_builder.end());
        ImGui::Text("#weights (connections): %d", nn.num_weights());
        ImGui::Text("#biases (neurons): %d", nn.num_biases());
        if (ImGui::SliderInt("% weights shown", &percent_weights_shown, 1, 100)) {
            create_connection_matrices(neuron_connection_matrices, nn, max_layer_size);
        }
    }
    ImGui::End();
}

void fps_overlay(bool* p_open, float fps) {
    auto window_flags = imgui_window_overlay();
    if (ImGui::Begin("FPS", p_open, window_flags)) {
        ImGui::Text("FPS: %d", (int) fps);
    }
    ImGui::End();
}

void evaluation_window(bool* p_open, bool available, GLuint image_texture, int predicted, float predicted_confidence, int actual) {
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize;

    if (ImGui::Begin("Evaluation", p_open, window_flags)) {
        if (available) {
            ImGui::Text("Predicted: %d", predicted);
            ImGui::Text("Confidence: %3.1f%%", predicted_confidence * 100.0f);
            ImGui::Text("Actual: %d", actual);
            ImGui::Image((void*) (intptr_t) image_texture, ImVec2(image_length * 10, image_length * 10));
        }
        else {
            ImGui::Text("No data loaded.");
        }
        ImGui::End();
    }
}

void create_texture(GLuint* out_texture) {
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);

    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	// set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    *out_texture = image_texture;
}

void set_texture_to_blank() {
    unsigned char image_data[image_length * image_length * 3];
    for (auto i = 0; i < image_length; ++i) {
        for (auto j = 0; j < image_length; ++j) {
            image_data[(i * image_length + j) * 3] = 0;
            image_data[(i * image_length + j) * 3 + 1] = 0;
            image_data[(i * image_length + j) * 3 + 2] = 0;
        }
    }

#if defined(GL_UNPACK_ROW_LENGTH)
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
#endif
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image_length, image_length, 0, GL_RGB, GL_UNSIGNED_BYTE, image_data);
}

int main()
{
    // load training data

    std::vector<net_matrix> training_data_input, training_data_expected;
    std::vector<net_matrix> test_data_input, test_data_expected;
    std::vector<net_matrix> validation_data_input, validation_data_expected;

    // model info:

    bool has_data_loaded = false;
    int max_mnist_items = 60000;
    int training_size = 59000;

    int epochs = 1;
    int mini_batch_size = 192;
    float eta = 3.0f;
    int num_backprop_threads = 16;
    int num_backprop_per_thread = 12;
    int num_evaluate_threads = 16;
    int num_feedforward_per_thread = 800;

    std::random_device rd{};
    std::mt19937 gen{ rd() };
    auto nn = network{ { image_length * image_length, 30, 10 }, gen };
    std::vector<net_matrix> nn_activations = nn.create_activations(0, allocator);

    size_t num_layers = nn.layer_sizes.size(), max_layer_size = nn.max_layer_size();
    std::vector<neuron_connection_matrices_tuple_t> neuron_connection_matrices; // { model matrix, weight }
    create_connection_matrices(neuron_connection_matrices, nn, static_cast<float>(max_layer_size));

    int predicted_classification = 0, actual_classification = 0;
    float predicted_confidence = 0.0f;

    bool show_demo_window = false;

    bool show_create_window = false;
    bool show_open_window = false;
    bool show_save_window = false;
    bool show_data_window = true;
    bool show_training_window = false;
    bool show_info_overlay = false;
    bool show_fps_overlay = false;
    bool show_evaluation_window = false;

    std::vector<size_t> create_sizes;
    create_sizes.resize(3);
    create_sizes[0] = image_length * image_length;
    create_sizes[1] = 10;
    create_sizes[2] = 10;

    std::string open_save_path = std::filesystem::current_path().string() + "\\my.nn";

    std::chrono::steady_clock::time_point tp_fps = std::chrono::steady_clock::now();
    const std::chrono::milliseconds fps_refresh_rate{ 100 };
    size_t frames_since_refresh = 0;
    float fps = 0.0f;

    // glfw: initialize and configure
    // ------------------------------
    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;
    const char* glsl_version = "#version 330 core";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(start_window_width, start_window_height, "NeuralNets", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetScrollCallback(window, mouse_scroll_callback);
    glfwSetCursorPosCallback(window, mouse_cursor_pos_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glEnable(GL_BLEND); //Enable blending.
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Set blending function.

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void) io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // build and compile our shader zprogram
    // ------------------------------------
    Shader networkShader("./network.vs", "./network.fs");
    Shader connectionShader("./connection.vs", "./connection.fs");

    size_t circle_num_outer_vertices = 12;
    const size_t num_circle_vertices = (circle_num_outer_vertices + 1) * 4;
    float* circle_vertices = new float[num_circle_vertices];
    const size_t num_indices = circle_num_outer_vertices * 3;
    uint* circle_indices = new uint[num_indices];

    circle_vertices[0] = circle_vertices[1] = circle_vertices[2] = circle_vertices[3] = 0.0f;
    for (auto i = 0; i < circle_num_outer_vertices; ++i) {
        float radians = 2.0f * glm::pi<float>() * i / circle_num_outer_vertices;
        circle_vertices[(i + 1) * 4] = glm::cos(radians) / 2.0f;
        circle_vertices[(i + 1) * 4 + 1] = glm::sin(radians) / 2.0f;
        circle_vertices[(i + 1) * 4 + 2] = circle_vertices[(i + 1) * 4] + 0.5f;
        circle_vertices[(i + 1) * 4 + 3] = circle_vertices[(i + 1) * 4 + 1] + 0.5f;
        circle_indices[i * 3] = 0;
        circle_indices[i * 3 + 1] = i + 1;
        circle_indices[i * 3 + 2] = (i + 1) % circle_num_outer_vertices + 1;
    }

    uint circle_vbo, circle_vao, circle_ebo;
    glGenVertexArrays(1, &circle_vao);
    glGenBuffers(1, &circle_vbo);
    glGenBuffers(1, &circle_ebo);

    glBindVertexArray(circle_vao);

    glBindBuffer(GL_ARRAY_BUFFER, circle_vbo);
    glBufferData(GL_ARRAY_BUFFER, num_circle_vertices * sizeof(float), circle_vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, circle_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, num_indices * sizeof(uint), circle_indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) 0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*) (2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    float square_vertices[] = {
        // positions          // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 1.0f, // top right
         0.5f, -0.5f, 0.0f,   1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f,   0.0f, 1.0f  // top left 
    };
    unsigned int square_indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };
    unsigned int square_vbo, square_vao, square_ebo;
    glGenVertexArrays(1, &square_vao);
    glGenBuffers(1, &square_vbo);
    glGenBuffers(1, &square_ebo);

    glBindVertexArray(square_vao);

    glBindBuffer(GL_ARRAY_BUFFER, square_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(square_vertices), square_vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, square_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(square_indices), square_indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    //stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.

    GLuint image_texture;
    create_texture(&image_texture);
    set_texture_to_blank();

    auto create_tex = [&](const net_matrix& test_activation, bool predicted_correct) {
        if (!test_data_input.empty())
        {
            glBindTexture(GL_TEXTURE_2D, image_texture);
            unsigned char image_data[image_length * image_length * 3];

            for (size_t x = 0; x < image_length; ++x) {
                for (size_t y = 0; y < image_length; ++y) {
                    auto pixel = (unsigned char)(test_activation.at(0, (image_length - 1 - x) * image_length + y) * 255.0f);
                    if (!predicted_correct)
                        pixel = (unsigned char)255 - pixel;
                    auto array_idx = ((image_length - x - 1) * image_length + y) * 3;
                    image_data[array_idx] = image_data[array_idx + 1] = image_data[array_idx + 2] = pixel;
                }
            }

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image_length, image_length, 0, GL_RGB, GL_UNSIGNED_BYTE, image_data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }
        else
        {
            std::cout << "Failed to load texture" << std::endl;
        }
    };

    size_t image_idx = 0;
    std::chrono::steady_clock::time_point tp_refresh_texture = std::chrono::steady_clock::now();
    const auto tp_refresh_duration = std::chrono::seconds{ 1 };

    // wireframe mode:
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        processInput(window, &show_demo_window);

        // find network's prediction for current example and its correctness.
        auto refresh_now = std::chrono::steady_clock::now();
        if (has_data_loaded) {
            if (refresh_now - tp_refresh_texture > tp_refresh_duration) {
                image_idx = (image_idx + 1) % test_data_input.size();

                std::vector<net_matrix> weights_transposed;
                weights_transposed.reserve(nn.weights.size());
                for (const auto& weight_mat : nn.weights) {
                    net_matrix& weight_transposed = weights_transposed.emplace_back(weight_mat.rows, weight_mat.columns, allocator.allocate(weight_mat.rows * weight_mat.columns));
                    weight_transposed.set_transpose(weight_mat);
                }

                // calculate the network's prediction for this image.
                auto& test_activation = test_data_input[image_idx];
                auto& classification = test_data_expected[image_idx];
                nn_activations.front() = test_activation;
                nn.feedforward(weights_transposed, nn_activations);
                const auto& final_activation = nn_activations.back();
                auto it_predicted = std::max_element(final_activation.begin(), final_activation.end());
                predicted_classification = static_cast<int>(std::distance(final_activation.begin(), it_predicted));
                predicted_confidence = *it_predicted;
                actual_classification = static_cast<int>(std::distance(classification.begin(), std::max_element(classification.begin(), classification.end())));
                bool predicted_correct = actual_classification == predicted_classification;

                create_tex(test_activation, predicted_correct);

                tp_refresh_texture = refresh_now;
            }
        }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        if (ImGui::BeginMainMenuBar()) {
            if (ImGui::BeginMenu("File")) {
                ImGui::MenuItem("New", nullptr, &show_create_window);
                ImGui::MenuItem("Open", nullptr, &show_open_window);
                ImGui::MenuItem("Save", nullptr, &show_save_window);
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("NN")) {
                ImGui::MenuItem("Data", nullptr, &show_data_window);
                ImGui::MenuItem("Train", nullptr, &show_training_window);
                ImGui::MenuItem("Evaluation", nullptr, &show_evaluation_window);
                ImGui::MenuItem("Info", nullptr, &show_info_overlay);
                ImGui::EndMenu();
            }
            if (ImGui::BeginMenu("Debug")) {
                ImGui::MenuItem("FPS", nullptr, &show_fps_overlay);
                ImGui::EndMenu();
            }
            ImGui::EndMainMenuBar();
        }

        if (show_create_window)
            create_window(&show_create_window, create_sizes, nn, nn_activations, neuron_connection_matrices, num_layers, max_layer_size);

        if (show_open_window)
            open_window(&show_open_window, &open_save_path, nn, nn_activations, neuron_connection_matrices, num_layers, max_layer_size);

        if (show_save_window)
            save_window(&show_save_window, &open_save_path, nn);

        if (show_data_window)
            data_window(&show_data_window, training_data_input, training_data_expected, test_data_input, test_data_expected, validation_data_input, validation_data_expected,
                &max_mnist_items, &training_size, has_data_loaded);

        if (show_training_window)
            training_window(&show_training_window, nn,
                training_data_input, training_data_expected, test_data_input, test_data_expected, validation_data_input, validation_data_expected,
                &epochs, &mini_batch_size, &eta,
                &num_backprop_threads, &num_backprop_per_thread, &num_evaluate_threads, &num_feedforward_per_thread);

        if (show_info_overlay)
            info_overlay(&show_info_overlay, nn, neuron_connection_matrices, static_cast<float>(max_layer_size));

        if(show_fps_overlay)
            fps_overlay(&show_fps_overlay, fps);

        if (show_evaluation_window)
            evaluation_window(&show_evaluation_window, has_data_loaded, image_texture, predicted_classification, predicted_confidence, actual_classification);

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        if (show_demo_window)
            ImGui::ShowDemoWindow(&show_demo_window);

        // render
        // ------
        ImGui::Render();

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // draw the neural network:

        int window_width, window_height;
        glfwGetWindowSize(window, &window_width, &window_height);
        window_dimensions = glm::vec2(window_width, window_height);

        auto layer_view_dimensions = glm::vec2(
            num_layers * (1.0f + x_spacing),
            max_layer_size * (1.0f + y_spacing)
        );

        //auto aspect_ratio = (float) window_width / window_height;
        //auto camera_dimensions = glm::vec2(layer_view_dimensions.y * aspect_ratio, layer_view_dimensions.y);
        glm::vec2 camera_dimensions = window_dimensions / reference_pixels_per_neuron * zoom_factor;

        glm::mat4 mat_view = glm::mat4(1.0f);
        mat_view = glm::translate(mat_view, glm::vec3(
            -layer_view_dimensions.x / 2.0f + mouse_pan.x,
            -layer_view_dimensions.y / 2.0f + mouse_pan.y,
            0.0f
        ));

        glm::mat4 mat_projection = glm::ortho(
            -camera_dimensions.x / 2.0f, camera_dimensions.x / 2.0f,
            -camera_dimensions.y / 2.0f, camera_dimensions.y / 2.0f,
            -1.0f, 1.0f);

        connectionShader.use();
        connectionShader.setMat4("projection", mat_projection * mat_view);
        auto time_in_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(refresh_now - tp_refresh_texture).count() / (float) std::chrono::duration_cast<std::chrono::milliseconds>(tp_refresh_duration).count();
        auto time_per_layer = 1.0f / (nn.layer_sizes.size() - 1);

        // draw connections between neurons:
        glBindVertexArray(square_vao);

        if (has_data_loaded) {
            for (const auto& [model_mat, from_x, from_y, to_y, weight] : neuron_connection_matrices) {
                connectionShader.setMat4("model", model_mat);
                connectionShader.setVec4("color", glm::vec4(weight, 0.0f, 1.0f - weight, connection_alpha));

                const auto to_x = from_x + 1;

                auto activation_position = time_in_seconds * (nn.layer_sizes.size() - 1) - from_x;
                connectionShader.setFloat("activation_position", activation_position);
                auto from_activation = nn_activations[from_x].at(0, from_y);
                auto to_activation = nn_activations[to_x].at(0, to_y);
                connectionShader.setFloat("activation_strength", std::clamp(from_activation * to_activation, 0.0f, 1.0f));

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
            }
        }
        else {
            for (const auto& [model_mat, from_x, from_y, to_y, weight] : neuron_connection_matrices) {
                connectionShader.setMat4("model", model_mat);
                connectionShader.setVec4("color", glm::vec4(weight, 0.0f, 1.0f - weight, connection_alpha));
                connectionShader.setFloat("activation_position", -1.0f);
                connectionShader.setFloat("activation_strength", 0.0f);

                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
            }
        }


        networkShader.use();
        networkShader.setMat4("projection", mat_projection * mat_view);

        // draw the neurons:
        glBindVertexArray(circle_vao);
        if (has_data_loaded) {
            // draw the neurons with their activation as their color
            for (size_t x = 0; x < nn.layer_sizes.size(); ++x) {
                auto layer_size = nn.layer_sizes[x];
                for (size_t y = 0; y < layer_size; ++y) {
                    glm::mat4 mat_model = glm::mat4(1.0f);

                    float animate = std::clamp(1.5f * (1.0f / 1.5f - std::abs(time_in_seconds - time_per_layer * x)), 0.0f, 1.0f);

                    mat_model = glm::translate(mat_model, position_for_neuron(static_cast<float>( max_layer_size ), static_cast<float>( x ), static_cast<float>( y ), static_cast<float>( layer_size )));
                    networkShader.setMat4("model", mat_model);
                    glm::vec4 bias_color;
                    if (x > 0) {
                        auto bias = x > 0 ? nn.biases[x - 1].at(0, y) : 0.0f;
                        bias_color = glm::vec4(bias, 0.0f, 1.0f - bias, 1.0f);
                    }
                    else {
                        bias_color = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                    auto activation = nn_activations[x].at(0, y);
                    auto activation_color = glm::vec4(activation, activation, activation, 1.0f);
                    networkShader.setVec4("color", glm::mix(bias_color, activation_color, animate));
                    glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(num_indices), GL_UNSIGNED_INT, 0);
                }
            }
        }
        else {
            // draw the neurons with their biases as their color
            for (size_t x = 1; x < nn.layer_sizes.size(); ++x) {
                auto layer_size = nn.layer_sizes[x];
                for (size_t y = 0; y < layer_size; ++y) {
                    glm::mat4 mat_model = glm::mat4(1.0f);

                    mat_model = glm::translate(mat_model, position_for_neuron(static_cast<float>( max_layer_size ), static_cast<float>( x ), static_cast<float>( y ), static_cast<float>( layer_size )));
                    networkShader.setMat4("model", mat_model);
                    auto bias = nn.biases[x - 1].at(0, y);
                    auto bias_color = glm::vec4(bias, 0.0f, 1.0f - bias, 1.0f);
                    networkShader.setVec4("color", bias_color);
                    glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(num_indices), GL_UNSIGNED_INT, 0);
                }
            }
        }

        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);

        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        ++frames_since_refresh;

        auto tp_frame_end = std::chrono::steady_clock::now();
        if (tp_frame_end - tp_fps > fps_refresh_rate) {
            auto frame_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(tp_frame_end - tp_fps).count();
            fps = 1000.0f / frame_milliseconds * (float) frames_since_refresh;
            frames_since_refresh = 0;
            tp_fps = tp_frame_end;
        }
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &square_vao);
    glDeleteBuffers(1, &square_vbo);
    glDeleteBuffers(1, &square_ebo);

    glDeleteVertexArrays(1, &circle_vao);
    glDeleteBuffers(1, &circle_vbo);
    glDeleteBuffers(1, &circle_ebo);

    delete[] circle_vertices;
    delete[] circle_indices;

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow* window, bool* show_demo_window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    static int key_d_last_press = GLFW_RELEASE;
    int key_d_press = glfwGetKey(window, GLFW_KEY_D);
    if ((glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS) && key_d_press == GLFW_PRESS && key_d_last_press == GLFW_RELEASE)
        *show_demo_window = !*show_demo_window;
    key_d_last_press = key_d_press;
}

void mouse_scroll_callback(GLFWwindow* /*window*/, double /*x*/, double y) {
    zoom_factor *= (1.0f - static_cast<float>(y) * scroll_sensitivity);
}

void mouse_cursor_pos_callback(GLFWwindow* window, double xpos, double ypos) {
    if (first_mouse) {
        mouse_last_pos.x = static_cast<float>(xpos);
        mouse_last_pos.y = static_cast<float>(ypos);
        first_mouse = false;
    }

    float xoffset = static_cast<float>(xpos) - mouse_last_pos.x;
    float yoffset = mouse_last_pos.y - static_cast<float>(ypos);
    mouse_last_pos.x = static_cast<float>(xpos);
    mouse_last_pos.y = static_cast<float>(ypos);

    static int mouse_last_button = GLFW_RELEASE;
    int mouse_button = glfwGetMouseButton(window, 0);
    if (mouse_last_button == GLFW_PRESS && mouse_button == GLFW_PRESS) {
        // apply the panning in camera dimensions.
        mouse_pan.x += xoffset / reference_pixels_per_neuron * zoom_factor;
        mouse_pan.y += yoffset / reference_pixels_per_neuron * zoom_factor;
    }
    mouse_last_button = mouse_button;
}

void framebuffer_size_callback(GLFWwindow* /*window*/, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

void glfw_error_callback(int error, const char* description) {
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

