#include <iostream>
#include <future>
#include <shared_mutex>
#include "net.h"
#include "simple_log_timer.h"

#if _WIN32
#define NOMINMAX
#include <winsock.h>
#endif

inline float sigmoid(float z) {
	return 1.0f / (1.0f + std::exp(-z));
}

inline float sigmoid_prime(float z) {
	return sigmoid(z) * (1.0f - sigmoid(z));
}

// executes (up to) num_per_thread elements per task across as many as num_threads threads.
// task_function(Iter, Iter) -> R
// integrate(R&, R&&) -> void
template<typename Iter, typename TF, typename R, typename IF>
R perform_task_multithreaded(Iter begin, Iter end, TF&& task_function, R&& init_value, IF&& integrate, size_t num_threads, size_t num_per_thread) {
	const size_t size = std::distance(begin, end);
	if (num_threads <= 1 || size < num_per_thread) {
		return task_function(begin, end);
	}
	else {
		typedef std::packaged_task<R(Iter, Iter)> packaged_task_t;
		typedef std::future<R> future_t;

		std::vector<std::pair<std::thread, future_t>> threads;
		threads.reserve(num_threads);

		auto queue_task = [&threads, &task_function](Iter task_begin, Iter task_end) {
			packaged_task_t task{ task_function };
			auto future = task.get_future();
			threads.emplace_back(std::thread{ std::move(task), task_begin, task_end }, std::move(future));
		};

		auto num_iterations = size / num_per_thread;
		size_t idx = 0;
		auto it = begin;

		auto queue_up_to_max_threads = [&]() {
			for (; threads.size() < num_threads && idx < num_iterations; ++idx, it += num_per_thread) {
				queue_task(it, it + num_per_thread);
			}
			// are less than num_per_thread tasks available?
			if (threads.size() < num_threads && it != end) {
				queue_task(it, end);
				it = end;
			}
		};

		queue_up_to_max_threads();

		R result = std::forward<R>(init_value);

		while (!threads.empty()) {
			threads.erase(std::remove_if(threads.begin(), threads.end(),
				[&result, &integrate](std::pair<std::thread, future_t>& pair) {
					auto& [thrd, future] = pair;
					if (future.wait_for(std::chrono::nanoseconds{ 0 }) == std::future_status::ready) {
						thrd.join();
						integrate(result, future.get());

						return true;
					}
					else {
						return false;
					}
				}),
				threads.end());

			queue_up_to_max_threads();
		}

		return result;
	}
}

static void reset_weights_and_biases(std::vector<net_matrix>& weights, std::vector<net_matrix>& biases, std::span<const size_t> sizes, network::allocator_type allocator, net_matrix::value_type default_value) {
	for (auto& mat : weights)
		allocator.deallocate(mat.cells, mat.size());
	for (auto& mat : biases)
		allocator.deallocate(mat.cells, mat.size());

	weights.clear();
	biases.clear();
	if (sizes.size() > 1) {
		weights.reserve(sizes.size() - 1);
		biases.reserve(sizes.size() - 1);
		for (auto it_prev_size = sizes.begin(), it_next_size = sizes.begin() + 1; it_next_size != sizes.end(); ++it_prev_size, ++it_next_size) {
			auto col_size = *it_prev_size, row_size = *it_next_size;
			auto& weight_mat = weights.emplace_back(col_size, row_size, allocator.allocate(col_size * row_size));
			for (auto& cell : weight_mat)
				new (&cell) net_matrix::value_type{ default_value };
			auto& biases_mat = biases.emplace_back(1, row_size, allocator.allocate(row_size));
			for (auto& cell : biases_mat)
				new (&cell) net_matrix::value_type{ default_value };
		}
	}
}

static void reset_weights_and_biases(std::vector<net_matrix>& weights, std::vector<net_matrix>& biases, std::span<const size_t> sizes, network::allocator_type allocator, uninitialized_t) {
	for (auto& mat : weights)
		allocator.deallocate(mat.cells, mat.size());
	for (auto& mat : biases)
		allocator.deallocate(mat.cells, mat.size());

	weights.clear();
	biases.clear();
	if (sizes.size() > 1) {
		weights.reserve(sizes.size() - 1);
		biases.reserve(sizes.size() - 1);
		for (auto it_prev_size = sizes.begin(), it_next_size = sizes.begin() + 1; it_next_size != sizes.end(); ++it_prev_size, ++it_next_size) {
			auto col_size = *it_prev_size, row_size = *it_next_size;
			weights.emplace_back(col_size, row_size, allocator.allocate(col_size * row_size));
			biases.emplace_back(1, row_size, allocator.allocate(row_size));
		}
	}
}

network::network(std::span<const size_t> sizes, uninitialized_t) {
	reset(sizes, uninitialized);
}

network::network(std::span<const size_t> sizes, net_matrix::value_type default_value) {
	reset(sizes, default_value);
}

network::network(std::span<const size_t> sizes, std::mt19937& gen) {
	reset(sizes, uninitialized);

	std::normal_distribution<float> nd;

		for (auto& w_m : weights) {
			for (auto& w_c : w_m) {
				w_c = nd(gen);
				}
			}

		for (auto& b_m : biases) {
			for (auto& b_c : b_m) {
				b_c = nd(gen);
			}
		}
	}

void network::reset(std::span<const size_t> sizes, net_matrix::value_type default_value) {
	layer_sizes.resize(sizes.size());
	std::copy(sizes.begin(), sizes.end(), layer_sizes.begin());

	reset_weights_and_biases(weights, biases, sizes, allocator, default_value);
}

void network::reset(std::span<const size_t> sizes, uninitialized_t) {
	layer_sizes.resize(sizes.size());
	std::copy(sizes.begin(), sizes.end(), layer_sizes.begin());

	reset_weights_and_biases(weights, biases, sizes, allocator, uninitialized);
}

std::vector<net_matrix> network::create_activations(size_t start_layer_idx, allocator_type alloc) const
{
	std::vector<net_matrix> activations;
	activations.reserve(layer_sizes.size() - start_layer_idx);
	for (auto it = layer_sizes.begin() + start_layer_idx; it != layer_sizes.end(); ++it)
		activations.emplace_back(1, *it, alloc.allocate(*it));

	return activations;
}

void network::feedforward(std::span<const net_matrix> weights_transposed, std::span<net_matrix> activations, size_t in_layer_idx) const {
	auto it_weights_transposed = weights_transposed.begin() + in_layer_idx;
	auto it_biases = biases.begin() + in_layer_idx;
	auto it_activation_input = activations.begin();
	for (; it_activation_input + 1 != activations.end(); ++it_weights_transposed, ++it_biases, ++it_activation_input) {
		const net_matrix& mat_weight_transposed = *it_weights_transposed;
		const net_matrix& mat_biases = *it_biases;
		const net_matrix& activation_input = *it_activation_input;
		auto& activation_output = *(it_activation_input + 1);

		(activation_output.set_transpose_multiply(mat_weight_transposed, activation_input) += mat_biases).element_transform(sigmoid);
	}
}

void network::stochastic_gradient_descent(std::span<net_matrix> training_data_input, std::span<net_matrix> training_data_expected,
		size_t epochs, size_t mini_batch_size, float eta, size_t sgd_num_threads, size_t sgd_num_per_thread,
		std::span<const net_matrix> test_data_input, std::span<const net_matrix> test_data_expected, size_t evaluate_num_threads, size_t evaluate_num_per_thread, allocator_type alloc) {

	if (epochs == 0 || training_data_input.empty())
		return;

	auto integrate_results = [](std::vector<net_matrix>& result_weights, std::vector<net_matrix>& result_biases, const std::vector<net_matrix>& to_add_weights, const std::vector<net_matrix>& to_add_biases) {
		for (auto i = 0; i < result_biases.size(); ++i) {
			result_biases[i] += to_add_biases[i];
		}

		for (auto i = 0; i < result_weights.size(); ++i) {
			result_weights[i] += to_add_weights[i];
		}
	};

	enum class threads_state {
		workers_processing_data,
		coordinator_processing_data,
		finish,
	};

	std::random_device rd;
	std::mt19937 gen(rd());

	size_t batch_size = 0;

	std::mutex iterator_mutex;
	auto it_training_input = training_data_input.begin();
	auto it_training_input_end = training_data_input.end();
	auto it_training_expected = training_data_expected.begin();

	std::mutex result_mutex;
	std::vector<net_matrix> nablas_weights, nablas_biases;

	std::condition_variable state_cv;
	std::mutex state_mutex;
	threads_state threading_state = threads_state::coordinator_processing_data;

	std::condition_variable processor_cv;
	std::mutex processor_mutex;
	size_t num_processors_waiting = 0;

	std::vector<std::thread> threads;
	threads.reserve(sgd_num_threads);

	for (size_t i = 0; i < sgd_num_threads; ++i) {
		auto thread_func = [this, eta, sgd_num_per_thread, &alloc, &processor_mutex, &processor_cv, &num_processors_waiting, &state_mutex, &state_cv, &threading_state,
				&batch_size, &iterator_mutex, &it_training_input, &it_training_input_end, &it_training_expected, &result_mutex, &nablas_weights, &nablas_biases, &integrate_results]() {
			std::span<net_matrix>::iterator training_batch_input_begin, training_batch_input_end, training_batch_expected_begin;

			std::unique_lock<std::mutex> state_lock{ state_mutex };
			state_cv.wait(state_lock, [&threading_state]() {
				return threading_state != threads_state::coordinator_processing_data;
			});

			while(threading_state == threads_state::workers_processing_data) {
				state_lock.unlock();
				//std::cout << "thread " << std::this_thread::get_id() << " starting" << std::endl;

				while (true) {
					{
						std::lock_guard iterator_lock{ iterator_mutex };
						if(it_training_input == it_training_input_end)
							break;

						training_batch_input_begin = it_training_input;
						training_batch_expected_begin = it_training_expected;
						auto min_size = std::min(sgd_num_per_thread, (size_t) std::distance(it_training_input, it_training_input_end));
						it_training_input += min_size;
						training_batch_input_end = it_training_input;
						it_training_expected += min_size;
					}

					//std::cout << "thread " << std::this_thread::get_id() << " working" << std::endl;

					auto to_add = update_mini_batch(training_batch_input_begin, training_batch_input_end, training_batch_expected_begin, batch_size, eta, alloc);

					{
						std::lock_guard result_lock{ result_mutex };
						integrate_results(nablas_weights, nablas_biases, to_add.first, to_add.second);
					}

					//std::cout << "thread " << std::this_thread::get_id() << " integrated work" << std::endl;
				}

				// signal complete
				{
					std::lock_guard processor_lock{ processor_mutex };
					++num_processors_waiting;
				}
				processor_cv.notify_all();

				// wait for instruction
				//std::cout << "thread " << std::this_thread::get_id() << " waiting for instruction" << std::endl;
				{
					std::unique_lock<std::mutex> processor_lock{ processor_mutex };
					processor_cv.wait(processor_lock, [&num_processors_waiting]() {
						return num_processors_waiting == 0;
						});
				}

				state_lock.lock();
				state_cv.wait(state_lock, [&threading_state]() {
					return threading_state != threads_state::coordinator_processing_data;
				});
			}

			//std::cout << "thread " << std::this_thread::get_id() << " finished" << std::endl;
		};

		threads.push_back(std::thread{ std::move(thread_func) });
	}

	for (size_t j = 0; j < epochs; ++j) {
		{
			//simple_log_timer nn_timer("shuffle", timer_precision::nanoseconds);
			
			// same as shuffle(), but performs the same shuffle operations on both training_data_input and training_data_expected
			// see https://en.cppreference.com/w/cpp/algorithm/random_shuffle for reference implementation of shuffle()
			typedef typename std::vector<net_matrix>::difference_type diff_t;
			typedef std::uniform_int_distribution<diff_t> distr_t;
			typedef typename distr_t::param_type param_t;

			distr_t distr;
			diff_t size = training_data_input.end() - training_data_input.begin();
			for (diff_t idx = size - 1; idx > 0; --idx) {
				using std::swap;
				diff_t idx_other = distr(gen, param_t(0, idx));
				swap(training_data_input[idx], training_data_input[idx_other]);
				swap(training_data_expected[idx], training_data_expected[idx_other]);
			}
		}

		for (size_t idx_batch_begin = 0; idx_batch_begin < training_data_input.size(); idx_batch_begin += mini_batch_size) {
			//std::cout << "coordinator setting up data" << std::endl;

			auto it_batch_input_begin = training_data_input.begin() + idx_batch_begin;
			auto it_batch_expected_begin = training_data_expected.begin() + idx_batch_begin;
			auto this_batch_size = std::min(mini_batch_size, training_data_input.size() - idx_batch_begin);

			reset_weights_and_biases(nablas_weights, nablas_biases, layer_sizes, alloc, 0);
			batch_size = this_batch_size;

			{
				std::lock_guard iterator_lock{ iterator_mutex };
				it_training_input = it_batch_input_begin;
				it_training_input_end = it_batch_input_begin + this_batch_size;
				it_training_expected = it_batch_expected_begin;
			}

			// signal workers that data is available
			{
				std::lock_guard state_lock{ state_mutex };
				threading_state = threads_state::workers_processing_data;
			}
			state_cv.notify_all();

			// wait until workers complete
			//std::cout << "coordinator waiting for workers" << std::endl;
			{
				std::unique_lock<std::mutex> processor_lock{ processor_mutex };
				processor_cv.wait(processor_lock, [&num_processors_waiting, sgd_num_threads]() {
					return num_processors_waiting == sgd_num_threads;
				});

				{
					std::lock_guard state_lock{ state_mutex };
					threading_state = threads_state::coordinator_processing_data;
				}
				state_cv.notify_all();

				num_processors_waiting = 0;
			}
			processor_cv.notify_all();

			//std::cout << "coordinator finalizing data" << std::endl;

			{
				//simple_log_timer nn_timer("combine", timer_precision::nanoseconds);
				for (auto i = 0; i < weights.size(); ++i) {
					biases[i] -= nablas_biases[i];
				}

				for (auto i = 0; i < biases.size(); ++i) {
					weights[i] -= nablas_weights[i];
				}
			}
		}

		std::cout << "Epoch " << j << " complete\n";
		if (!test_data_input.empty()) {
			//simple_log_timer nn_timer("evaluate", timer_precision::nanoseconds);
			auto num_correct = evaluate_accuracy(test_data_input, test_data_expected, evaluate_num_threads, evaluate_num_per_thread, alloc);
			std::cout << "Classified " << num_correct << " out of " << test_data_input.size() << " correctly\n";
		}
	}

	//std::cout << "coordinator finished" << std::endl;

	// signal workers to finish
	{
		std::lock_guard state_lock{ state_mutex };
		threading_state = threads_state::finish;
}
	state_cv.notify_all();

	for (auto& thread : threads)
		thread.join();
}

std::pair<std::vector<net_matrix>, std::vector<net_matrix>> network::update_mini_batch(
		std::span<net_matrix>::iterator training_batch_input_begin, std::span<net_matrix>::iterator training_batch_input_end,
		std::span<net_matrix>::iterator training_batch_expected_begin,
		size_t mini_batch_size, float eta, allocator_type alloc) const {
	// nabla is the name of the upside down triangle (capital delta).
	std::vector<net_matrix> nabla_weights;
	std::vector<net_matrix> nabla_biases;
	reset_weights_and_biases(nabla_weights, nabla_biases, layer_sizes, alloc, 0);

	std::vector<net_matrix> delta_nabla_weights;
	std::vector<net_matrix> delta_nabla_biases;
	reset_weights_and_biases(delta_nabla_weights, delta_nabla_biases, layer_sizes, alloc, uninitialized);

	std::vector<net_matrix> weights_transposed;
	weights_transposed.reserve(weights.size());
	for (const auto& weight_mat : weights) {
		net_matrix& weight_transposed = weights_transposed.emplace_back(weight_mat.rows, weight_mat.columns, alloc.allocate(weight_mat.rows * weight_mat.columns));
		weight_transposed.set_transpose(weight_mat);
	}

	for (auto it_batch_input = training_batch_input_begin, it_batch_expected = training_batch_expected_begin; it_batch_input != training_batch_input_end; ++it_batch_input, ++it_batch_expected) {
		auto& x = *it_batch_input;
		auto& y = *it_batch_expected;

		backpropagation(delta_nabla_weights, delta_nabla_biases, weights_transposed, x, y, alloc);

		for (auto i = 0; i < nabla_weights.size(); ++i) {
			nabla_weights[i] += delta_nabla_weights[i];
		}

		for (auto i = 0; i < nabla_biases.size(); ++i) {
			nabla_biases[i] += delta_nabla_biases[i];
		}
	}

#ifdef MATRIX_AVX_FLOAT
	auto eta_div_batch = eta / (float) mini_batch_size;
	auto eta_div_batch_avx = _mm256_set1_ps(eta_div_batch);
	for (auto i = 0; i < nabla_weights.size(); ++i) {
		nabla_weights[i].element_transform_avx(
			[eta_div_batch_avx](__m256 nw) { return _mm256_mul_ps(eta_div_batch_avx, nw); },
			[eta_div_batch](float nw) { return eta_div_batch * nw; });
	}

	for (auto i = 0; i < nabla_biases.size(); ++i) {
		nabla_biases[i].element_transform_avx(
			[eta_div_batch_avx](__m256 nb) { return _mm256_mul_ps(eta_div_batch_avx, nb); },
			[eta_div_batch](float nb) { return eta_div_batch * nb; });
	}
#else
	auto eta_div_batch = eta / (float) mini_batch_size;
	for (auto i = 0; i < nabla_weights.size(); ++i) {
		nabla_weights[i].element_transform([eta_div_batch](float nw) { return eta_div_batch * nw; });
	}

	for (auto i = 0; i < nabla_biases.size(); ++i) {
		nabla_biases[i].element_transform([eta_div_batch](float nb) { return eta_div_batch * nb; });
	}
#endif

	return std::make_pair(std::move(nabla_weights), std::move(nabla_biases));
}

void network::backpropagation(std::span<net_matrix> delta_nabla_weights, std::span<net_matrix> delta_nabla_biases, std::span<const net_matrix> weights_transposed, const net_matrix& x, const net_matrix& y, allocator_type alloc) const {
	std::vector<net_matrix> activations = create_activations(0, alloc);
	std::vector<net_matrix> zs = create_activations(1, alloc);

	// feed forward, but keep the intermediary zs matrix for later use.
	{
		activations.front() = x;

		auto it_weights = weights_transposed.begin();
		auto it_biases = biases.begin();
		auto it_activation_input = activations.begin();
		auto it_zs = zs.begin();
		for (; it_biases != biases.end(); ++it_weights, ++it_biases, ++it_activation_input, ++it_zs) {
			const net_matrix& weight_mat_transposed = *it_weights;
			const net_matrix& biases_mat = *it_biases;
			const net_matrix& activation_input = *it_activation_input;

			net_matrix& z = *it_zs;
			z.set_transpose_multiply(weight_mat_transposed, activation_input) += biases_mat;

			net_matrix& activation_output = *(it_activation_input + 1);
			activation_output.set_element_transform(z, sigmoid);
		}
	}

	// backward pass
	auto it_delta_nabla_weights = delta_nabla_weights.rbegin();
	auto it_delta_nabla_biases = delta_nabla_biases.rbegin();
	auto it_activation = activations.rbegin();
	auto it_zs = zs.rbegin();
	auto it_weights = weights.rbegin();
	{
		net_matrix& z_sigmoid_prime = *it_zs;
		z_sigmoid_prime.element_transform(sigmoid_prime);

		net_matrix& delta_nb = *it_delta_nabla_biases;
		delta_nb = *it_activation;
		(delta_nb -= y).element_multiply_assign(z_sigmoid_prime);
		delta_nb.transpose();

		net_matrix& prev_activation_transposed = *(it_activation + 1);
		prev_activation_transposed.transpose();

		net_matrix& delta_nw = *it_delta_nabla_weights;
		delta_nw.set_transpose_multiply(delta_nb, prev_activation_transposed);
		delta_nb.transpose();
	}

	for (; it_delta_nabla_weights + 1 != delta_nabla_weights.rend(); ++it_delta_nabla_weights, ++it_delta_nabla_biases, ++it_activation, ++it_zs, ++it_weights) {
		const net_matrix& next_delta_nb = *it_delta_nabla_biases;
		const net_matrix& next_weight = *it_weights;

		net_matrix& z_sigmoid_prime = *(it_zs + 1);
		z_sigmoid_prime.element_transform(sigmoid_prime);

		net_matrix& delta_nb = *(it_delta_nabla_biases + 1);
		delta_nb.set_transpose_multiply(next_weight, next_delta_nb).element_multiply_assign(z_sigmoid_prime);
		delta_nb.transpose();

		net_matrix& prev_activation_transposed = *(it_activation + 2);
		prev_activation_transposed.transpose();

		net_matrix& delta_nw = *(it_delta_nabla_weights + 1);
		delta_nw.set_transpose_multiply(delta_nb, prev_activation_transposed);
		delta_nb.transpose();
	}
}

size_t network::evaluate_accuracy(std::span<const net_matrix> test_data_input, std::span<const net_matrix> test_data_expected, size_t num_threads, size_t num_per_thread, allocator_type alloc) const {
	std::vector<net_matrix> weights_transposed;
	weights_transposed.reserve(weights.size());
	for (const auto& weight_mat : weights) {
		net_matrix& weight_transposed = weights_transposed.emplace_back(weight_mat.rows, weight_mat.columns, alloc.allocate(weight_mat.rows * weight_mat.columns));
		weight_transposed.set_transpose(weight_mat);
	}

	auto task_function = [this, &test_data_input, &test_data_expected, &alloc, &weights_transposed](std::span<const net_matrix>::iterator begin, std::span<const net_matrix>::iterator end) {
		std::vector<net_matrix> activations = create_activations(0, alloc);
		size_t local_count = 0;
		auto it_classification = test_data_expected.begin() + std::distance(test_data_input.begin(), begin);
		for (auto it_input = begin; it_input != end; ++it_input, ++it_classification) {
			const auto& test_activation = *it_input;
			const auto& classification = *it_classification;
			activations.front() = test_activation;
			feedforward(weights_transposed, activations);
			const auto& final_activation = activations.back();
			auto predicted_classification = std::distance(final_activation.begin(), std::max_element(final_activation.begin(), final_activation.end(), std::less<float>{}));
			auto actual_classification = std::distance(classification.begin(), std::max_element(classification.begin(), classification.end(), std::less<float>{}));
			if (predicted_classification == actual_classification)
				++local_count;
		}

		return local_count;
	};

	auto integrate = [](size_t& result, size_t to_add) { result += to_add; };
	return perform_task_multithreaded(test_data_input.begin(), test_data_input.end(), task_function, (size_t)0, integrate, num_threads, num_per_thread);
}

uint64_t htonll(uint64_t host) {
	const uint32_t high_part = htonl(static_cast<uint32_t>(host >> 32));
	const uint32_t low_part = htonl(static_cast<uint32_t>(host & 0xFFFFFFFFLL));
	return (static_cast<uint64_t>(low_part) << 32) | high_part;
}

uint64_t ntohll(uint64_t network) {
	uint32_t high_part = ntohl(static_cast<uint32_t>(network >> 32));
	uint32_t low_part = ntohl(static_cast<uint32_t>(network & 0xFFFFFFFFLL));
	return (static_cast<uint64_t>(low_part) << 32) | high_part;
}

std::ostream& operator<<(std::ostream& os, const network& net)
{
	uint64_t value;

	value = htonll(net.layer_sizes.size());
	os.write(reinterpret_cast<const char*>(&value), sizeof(value));

	for (auto size : net.layer_sizes) {
		value = htonll(size);
		os.write(reinterpret_cast<const char*>(&value), sizeof(value));
	}

	for (auto& w_m : net.weights) {
		for (auto& w_c : w_m) {
			os.write(reinterpret_cast<const char*>(&w_c), sizeof(w_c));
		}
	}

	for (auto& b_m : net.biases) {
		for (auto& b_c : b_m) {
			os.write(reinterpret_cast<const char*>(&b_c), sizeof(b_c));
		}
	}

	return os;
}

std::istream& operator>>(std::istream& is, network& net)
{
	uint64_t layer_count;
	is.read(reinterpret_cast<char*>(&layer_count), sizeof(layer_count));
	layer_count = ntohll(layer_count);

	net.layer_sizes.resize(layer_count);

	for (auto i = 0; i < layer_count; ++i) {
		uint64_t size;
		is.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.layer_sizes[i] = ntohll(size);
	}

	net.reset(net.layer_sizes, uninitialized);

	for (auto& w_m : net.weights) {
		for (auto& w_c : w_m) {
			is.read(reinterpret_cast<char*>(&w_c), sizeof(w_c));
		}
	}

	for (auto& b_m : net.biases) {
		for (auto& b_c : b_m) {
			is.read(reinterpret_cast<char*>(&b_c), sizeof(b_c));
		}
	}

	// if deserialization fails:
	// is.setstate(std::ios::failbit);

	return is;
}
